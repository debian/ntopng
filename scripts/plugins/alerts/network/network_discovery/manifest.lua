--
-- (C) 2020 - ntop.org
--

return {
  title = "Network Discovery check",
  description = "Detects if there is a Network Discovery running in a Network Interface",
  author = "ntop",
  dependencies = {},
}
