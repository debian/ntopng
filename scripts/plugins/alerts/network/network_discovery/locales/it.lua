--
-- (C) 2020 - ntop.org
--

return {
   network_discovery_description = "Esegue un allarme quando viene eseguita una Scoperta della Rete",
   network_discovery_title = "Scoperta della Rete Identificata",

   network_discovery_alert_description = "Scoperta della Rete periodica eseguita",
}
